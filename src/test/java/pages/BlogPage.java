package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class BlogPage {
    WebDriver driver = null;

    public BlogPage(WebDriver driver){
        PageFactory.initElements(driver, this);
        this.driver=driver;
    }

    @FindBy(xpath = "//a[@href='/blog/lista' and text()='Najnowsze']")
    private WebElement zobaczWiecejButton;

    @FindBy(xpath = "//a[@class='blog_menu_main_category js-article-menu-category']//span[text()='PWA']")
    private WebElement PWAButton;

    private WebElement activebutton(String nameOfButton){
      return   driver.findElement(By.xpath("//*[@class='blog_menu_main_category js-article-menu-category activeTag']//span[text()='"+nameOfButton+"']"));
    }

    public void goToLatest(){
        zobaczWiecejButton.click();
    }

    public void goToPWA(){
        PWAButton.click();
    }

    public boolean isButtonMenuActive(String button){
        return activebutton(button).isDisplayed();
    }

    public int numbOfArticle(){
        List<WebElement> article = driver.findElements(By.className("article_inner js-article_inner"));
        return article.size();
    }

    public int numbOfArticleWithPWATag(){
        List<WebElement> pwaTagInArticle = driver.findElements(By.xpath("//div[@class='article_inner js-article_inner']//div[@class='container-category-list']//a[@data-href='PWA']"));
        return pwaTagInArticle.size();
    }

    public void waitUntilTitleWillBe(String expectedTitle){
        WebDriverWait wait = new WebDriverWait(driver,15);
        wait.until(ExpectedConditions.titleIs(expectedTitle));
    }
}
