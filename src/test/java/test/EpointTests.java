package test;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.*;
import pages.BlogPage;
import pages.MainMenu;
import java.util.concurrent.TimeUnit;

public class EpointTests {

    WebDriver driver = null;

    @DataProvider(name = "addMethodDataProvider")
    public Object[][] dataProvider() {
        return new Object[][] {{"PWA","pwa"}};
    }

    @BeforeMethod
    public void setUpTest()
    {

        String projectPath = System.getProperty("user.dir");
        System.setProperty("webdriver.chrome.driver",  projectPath+"//drivers//chromedriver.exe");

        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        driver.get("https://www.e-point.pl/");
    }

    @Test(dataProvider ="addMethodDataProvider", invocationCount = 5)
    public void goToLatestPosts(String activeElementMenu, String nameOfTagArticle){

        MainMenu mainMenu = new MainMenu(driver);
        BlogPage blogPage = new BlogPage(driver);
        mainMenu.goToBlogPage();
        String expectedTitleBlogPage = "Blog | e-point SA";
        blogPage.waitUntilTitleWillBe(expectedTitleBlogPage);
        String actualTitleBlogPage = driver.getTitle();
        Assert.assertEquals(actualTitleBlogPage, expectedTitleBlogPage);
        blogPage.goToLatest();
        String actualTitlePage = driver.getTitle();
        String expectedTitlePage = "Lista news | e-point SA";
        Assert.assertEquals(actualTitlePage, expectedTitlePage);
        blogPage.goToPWA();
        Assert.assertTrue((blogPage.isButtonMenuActive(activeElementMenu)));
        String currentUrl = driver.getCurrentUrl();
        String expectedUrl = "https://www.e-point.pl/blog/lista?kategoria="+nameOfTagArticle;
        Assert.assertEquals(currentUrl, expectedUrl);
        Assert.assertEquals(blogPage.numbOfArticle(),blogPage.numbOfArticleWithPWATag());
    }

    @AfterMethod
    public void afterTest()
    {
        driver.quit();
    }
   }




